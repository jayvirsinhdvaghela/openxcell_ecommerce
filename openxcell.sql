-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2023 at 02:26 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `openxcell`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'LG', '1703160736.jpg', '2023-12-21 06:42:16', '2023-12-21 06:42:16'),
(2, 'Redmi', '1703160768.jpg', '2023-12-21 06:42:48', '2023-12-21 06:42:48'),
(3, 'Samsung', '1703160777.jpg', '2023-12-21 06:42:57', '2023-12-21 06:42:57'),
(4, 'Apple', '1703160789.jpg', '2023-12-21 06:43:09', '2023-12-21 06:43:09'),
(5, 'Motorola', '1703160899.jpg', '2023-12-21 06:44:59', '2023-12-21 06:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 2),
(6, '2023_12_21_085209_create_categories_table', 2),
(7, '2023_12_21_102406_create_products_table', 3),
(8, '2023_12_21_102418_create_product_images_table', 3),
(9, '2023_12_21_102425_create_product_attributes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `price` varchar(255) NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `title`, `description`, `price`, `is_published`, `created_at`, `updated_at`) VALUES
(3, 2, 'REDMI Note 12 5G (Mystique Blue, 128 GB)  (4 GB RAM)', '<ul>\r\n	<li>4 GB RAM | 128 GB ROM | Expandable Upto 1 TB</li>\r\n	<li>16.94 cm (6.67 inch) Full HD+ AMOLED Display</li>\r\n	<li>48MP + 8MP + 2MP | 13MP Front Camera</li>\r\n	<li>5000 mAh Battery</li>\r\n	<li>Qualcomm Snapdragon 4 Gen 1 Processor</li>\r\n</ul>', '20000', 1, '2023-12-21 06:48:18', '2023-12-21 06:48:18'),
(4, 1, 'LG G8X (Black, 128 GB)  (6 GB RAM)', '<ul>\r\n	<li>6 GB RAM | 128 GB ROM | Expandable Upto 2 TB</li>\r\n	<li>16.26 cm (6.4 inch) Full HD+ Display</li>\r\n	<li>12MP + 13MP | 32MP Front Camera</li>\r\n	<li>4000 mAh Battery</li>\r\n	<li>Qualcomm Snapdragon&trade; 855 Octa-core (up to 2.84 GHz x 1 + 2.42 GHz x 3 + 1.79 GHz x 4) Processor</li>\r\n	<li>Charging adapter is not included in the box</li>\r\n</ul>', '40000', 1, '2023-12-21 06:49:54', '2023-12-21 06:49:54'),
(5, 4, 'APPLE iPhone 14 (Blue, 256 GB)', '<ul>\r\n	<li>256 GB ROM</li>\r\n	<li>15.49 cm (6.1 inch) Super Retina XDR Display</li>\r\n	<li>12MP + 12MP | 12MP Front Camera</li>\r\n	<li>A15 Bionic Chip, 6 Core Processor Processor</li>\r\n</ul>', '60000', 1, '2023-12-21 06:51:43', '2023-12-21 06:51:43'),
(6, 4, 'APPLE iPhone 15 Pro Max (Natural Titanium, 256 GB)', '<ul>\r\n	<li>256 GB ROM</li>\r\n	<li>17.02 cm (6.7 inch) Super Retina XDR Display</li>\r\n	<li>48MP + 12MP + 12MP | 12MP Front Camera</li>\r\n	<li>A17 Pro Chip, 6 Core Processor Processor</li>\r\n</ul>', '80000', 1, '2023-12-21 07:30:50', '2023-12-21 07:30:50'),
(7, 3, 'SAMSUNG Galaxy S22 5G (Phantom White, 128 GB)  (8 GB RAM)', '<ul>\r\n	<li>8 GB RAM | 128 GB ROM</li>\r\n	<li>15.49 cm (6.1 inch) Full HD+ Display</li>\r\n	<li>50MP + 12MP + 10MP | 10MP Front Camera</li>\r\n	<li>3700 mAh Lithium-ion Battery</li>\r\n	<li>Snapdragon 8 Gen 1 Processor</li>\r\n</ul>', '36,999', 1, '2023-12-21 07:36:15', '2023-12-21 07:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 3, '3_01703161098.png', '2023-12-21 06:48:18', '2023-12-21 06:48:18'),
(2, 4, '4_01703161194.png', '2023-12-21 06:49:54', '2023-12-21 06:49:54'),
(3, 4, '4_11703161194.png', '2023-12-21 06:49:54', '2023-12-21 06:49:54'),
(4, 5, '5_01703161303.png', '2023-12-21 06:51:43', '2023-12-21 06:51:43'),
(5, 5, '5_11703161303.png', '2023-12-21 06:51:43', '2023-12-21 06:51:43'),
(6, 5, '5_21703161303.png', '2023-12-21 06:51:43', '2023-12-21 06:51:43'),
(7, 6, '6_01703163650.png', '2023-12-21 07:30:50', '2023-12-21 07:30:50'),
(8, 6, '6_11703163650.png', '2023-12-21 07:30:50', '2023-12-21 07:30:50'),
(9, 6, '6_21703163650.png', '2023-12-21 07:30:50', '2023-12-21 07:30:50'),
(10, 7, '7_01703163975.png', '2023-12-21 07:36:15', '2023-12-21 07:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `dial_code` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `type`, `email_verified_at`, `password`, `dial_code`, `phone_number`, `profile_pic`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@mailinator.com', 1, NULL, '$2y$12$uNpNAWkLD7gsNI/tom6vWu0q1hnp25olUBRVXaj5z7WDjTSBIFuK.', NULL, NULL, NULL, NULL, '2023-12-21 00:38:16', '2023-12-21 00:38:16'),
(2, 'User', 'user@mailinator.com', 0, NULL, '$2y$12$VUB6STXdUnNeN3XEAf6LyO9iXbO/BTnHZ5g2xXCjqRaeUJASV6nx2', NULL, NULL, NULL, NULL, '2023-12-21 00:47:45', '2023-12-21 00:47:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
