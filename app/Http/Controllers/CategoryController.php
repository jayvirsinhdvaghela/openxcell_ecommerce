<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\URL;
use Session;

class CategoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.category.category');
    } 
    
    public function show()
    {
        $data = Category::get();
        return DataTables::of($data)
            ->addColumn('actions', function ($category) {
                $html = '<a class="mr-2 btn btn-info btn-sm text-white"  href="' . route('admin.categories.edit', [encrypt($category->id)]) . '" id="'.$category->id.'" data-name="'. $category->category.'" title="Edit">Edit</a>';
                $html .= ' <a class="btn btn-danger btn-sm text-white deleteCategory" id="'.$category->id.'"title="Delete">Delete</a>';
                return $html;
            })
            ->addColumn('image', function ($category) {
                return '<img src="' . URL::to('/').'/img/category/'.$category->image . '" height="60" width="60"/>';

            })
            ->rawColumns(['actions','image'])
            ->addIndexColumn()
            ->make(true);
    } 

    public function destroy($id)
    {
        try {
            Category::where('id', $id)->delete();
            $response = array('success' => true, 'message' => "Category successfully deleted.");
            return response()->json($response);
        } catch (\Illuminate\Database\QueryException $ex) {
            $error_info = $ex->getMessage() . ' ' . $ex->getLine();
            $response = array('success' => false, 'message' => trans('common.something_went_wrong'), "info" => $error_info);
            return response()->json($response);
        } catch (\Exception $e) {
            $error_info = $e->getMessage() . ' ' . $e->getLine();
            $response = array('success' => false, 'message' => trans('common.something_went_wrong'), "info" => $error_info);
            return response()->json($response);
        }
    } 

    public function create()
    {
        return view('admin.category.create');
    }
    
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            if(request()->hasfile('avatar')){
                $avatarName = time().'.'.request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('img/category'), $avatarName);
            }
            Category::create([
                'name' => $input['name'],
                'image' => $avatarName,
            ]);

            Session::flash('flash_message', 'Category created successfully.');
            return redirect()->route('admin.category.categories');
        } catch (\Illuminate\Database\QueryException $ex) {
            $error_info = $ex->getMessage() . ' ' . $ex->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.categories');
        } catch (\Exception $e) {
            $error_info = $e->getMessage() . ' ' . $e->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.categories');
        }
    }

    public function edit($id)
    {
        $id = decrypt($id);
        $category = Category::find($id);

        return view('admin.category.edit',compact('category'));
    }

    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $avatarName = '';
            if(request()->hasfile('avatar')){
                $avatarName = time().'.'.request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('img/category'), $avatarName);
            }

            $category = Category::find($id);
            $category->name = $input['name'];
            if(!empty($avatarName))
            {
                $category->image = $avatarName;
            }
            $category->save();
            Session::flash('flash_message', 'Category updated successfully.');
            return redirect()->route('admin.categories');
        } catch (\Illuminate\Database\QueryException $ex) {
            $error_info = $ex->getMessage() . ' ' . $ex->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.categories');
        } catch (\Exception $e) {
            $error_info = $e->getMessage() . ' ' . $e->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.categories');
        }
    }
}

