<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImages;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\URL;
use Session;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.product.product');
    }

    public function show()
    {
        $data = Product::with('Images','Attributes')->get();
        return DataTables::of($data)
            ->addColumn('description', function ($data) {
                return html_entity_decode($data->description);
            })
            ->addColumn('category', function ($data) {
                return $data->category->name;
            })
            ->addColumn('is_published', function ($data) {
                if($data->is_published == 1){
                    return "Published";
                }
                return "Unpublished";
            })
            ->addColumn('images', function ($data) {
                $images = '';
                foreach ($data->Images as $key => $image) {
                    $images .= '<img style="margin-right:10px" src="' . URL::to('/').'/img/product/'.$image->image . '" height="60" width="60"/>';
                }
                return $images;
            })
            ->rawColumns(['category','is_published','images','description'])
            ->addIndexColumn()
            ->make(true);
    } 

    public function create()
    {
        $categories = Category::get();
        
        return view('admin.product.create',compact('categories'));
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();
            \DB::beginTransaction();

            $product = Product::create([
                'title' => $input['title'],
                'price' => $input['price'],
                'description' => $input['description'],
                'category_id' => $input['category'],
                'is_published' => $input['is_published'],
            ]);

            if(request()->hasfile('images')){
                $imageNames = [];
                foreach ($input['images'] as $key => $image) {
                    $avatarName = $product->id.'_'.$key.time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('img/product'), $avatarName);
                    $imageNames[] = $avatarName;
                }
            }

            foreach ($imageNames as $key => $imageName) {
                ProductImages::Create([
                    "product_id" => $product->id,
                    "image" => $imageName
                ]);
            }              

            \DB::commit();
            Session::flash('flash_message', 'Product created successfully.');
            return redirect()->route('admin.products');
        } catch (\Illuminate\Database\QueryException $ex) {
            \DB::rollBack();
            $error_info = $ex->getMessage() . ' ' . $ex->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.products');
        } catch (\Exception $e) {
            \DB::rollBack();
            $error_info = $e->getMessage() . ' ' . $e->getLine();
            Session::flash('flash_message', $error_info);
            return redirect()->route('admin.products');
        }
    }
}
