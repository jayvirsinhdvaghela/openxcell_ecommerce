<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function products($category_id)
    {
        $category_id = decrypt($category_id);
        $products = Product::Where('category_id',$category_id)->with('Images','Attributes')->get();
        
        return view('user.products',compact('products'));
    }

    public function product($product_id)
    {
        $product_id = decrypt($product_id);
        $product = Product::Where('id',$product_id)->with('Images','Attributes')->first();
        
        return view('user.product',compact('product'));
    }
}
