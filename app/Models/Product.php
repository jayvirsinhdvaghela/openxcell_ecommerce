<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'title',
        'price',
        'description',
        'is_published',
    ];

    public function Attributes() {
        return $this->hasMany('App\Models\ProductAttributes', 'product_id', 'id');
    }

    public function Images() {
        return $this->hasMany('App\Models\ProductImages', 'product_id', 'id');
    }

    public function Category() {
        return $this->belongsTo('App\Models\Category', 'category_id','id');
    }
}
