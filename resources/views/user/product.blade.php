@extends('layouts.app')
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <main class="container">

      <!-- Left Column / Headphones Image -->
      <div class="left-column">
        @php 
        $imgsrc = URL::to('/').'/img/product/'.$product->Images[0]->image;
        @endphp
        <img  src="{{ $imgsrc }}" alt="">
      </div>

      <div class="product-configuration">
          <!-- Cable Configuration -->
          <div class="cable-config">
            <span>All Images</span>
            
            @foreach($product->Images as $image)
                @php 
                    $imgsrc1 = URL::to('/').'/img/product/'.$image->image;
                @endphp

                    <img height="50px" width="50px" src="{{ $imgsrc1 }}" alt="">

            @endforeach
           
          </div>
        </div>


      <!-- Right Column -->
      <div class="right-column">

        <!-- Product Description -->
        <div class="product-description">
          <span>{{ $product->category->name }}</span>
          <h1>{{ $product->title }}</h1>
          <p>{!! $product->description !!}</p>
        </div>

        <!-- Product Configuration -->
    
        <!-- Product Pricing -->
        <div class="product-price">
          <span>{{ $product->price }}$</span>
          <a href="#" class="cart-btn">Add to cart</a>
        </div>

        


      </div>
    </main>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
