@extends('layouts.app')
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <div class="row" style="display: block;">
                        @foreach($categories as  $category)
                            @php 
                                $imgsrc = URL::to('/').'/img/category/'.$category->image;
                                $productRoute = route('user.products', [encrypt($category->id)]);
                            @endphp

                        <a  href="{{ $productRoute }}">
                            <div class="column">
                                <div class="card">
                                <h3>{{ $category->name }}</h3>
                                
                                <h3><img class="center" src="{{ $imgsrc }}" height="50px" width="50px"/></h3>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
