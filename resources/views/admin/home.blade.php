@extends('layouts.app')
  
@section('content')
<style>
.block {
  /* display: block; */
  width: 20%;
  border: none;
  background-color: #04AA6D;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #ddd;
  color: black;
}
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('admin.categories')}}"><button class="block">Categories</button></a>
                    <a href="{{route('admin.products')}}"><button class="block">Products</button></a>
                </div>
  
                <div class="card-body">
                    You are a Admin User.
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection