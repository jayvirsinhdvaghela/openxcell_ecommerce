$(document).ready(function () {

	CKEDITOR.replace('description',{height: "500px"});

    if ($('#category_table').length > 0) {
        $('#category_table').DataTable({   
            ajax: {
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                },
                url: base_url + "/admin/category/list",
            },
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 10,
            lengthMenu: [ 10, 25, 50, 75, 100 ],
            "order": [[ 0, "desc" ]],
            columns: [
                { data: 'DT_RowIndex', "orderable": false },
                {data: 'name'},
                {data: 'image'},
                {data: 'actions', orderable: false}
            ]
        });
    }
    $(document).on('click', '.deleteCategory', function () {
        let text = "Are you sure?";
        if (confirm(text) == true) {
		    var id = $(this).attr('id');
            $.ajax({
                url: base_url+"/admin/deletecategory/"+id,
                method: "POST",
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                },
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.success == true) {
                        toastr.success(response.message, "Success");
                        $('#category_table').DataTable().ajax.reload();
                    } else if (response.success == false) {
                        toastr.error(response.message, "Error");
                    }
                },
            });
        }
    });


    if ($('#products_table').length > 0) {
        $('#products_table').DataTable({   
            ajax: {
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                },
                url: base_url + "/admin/product/list",
            },
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 10,
            lengthMenu: [ 10, 25, 50, 75, 100 ],
            "order": [[ 0, "desc" ]],
            columns: [
                { data: 'DT_RowIndex', "orderable": false },
                {data: 'title'},
                {data: 'price'},
                {data: 'description'},
                {data: 'category'},
                {data: 'is_published'},
                {data: 'images'},
            ]
        });
    }

});
