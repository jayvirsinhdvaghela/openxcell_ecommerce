<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*------------------------------------------
--------------------------------------------
All Normal Users Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:user'])->group(function () {
  
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::controller(UserController::class)->group(function () {
        Route::get('products/{categoryid}',  'products')->name('user.products');
        Route::get('product/{id}',  'product')->name('user.product');
    });

});
  
/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:admin'])->group(function () {
    
    Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home');

    Route::prefix('admin')->controller(CategoryController::class)->group(function () {
        Route::get('categories',  'index')->name('admin.categories');
        Route::post('/category/list',  'show')->name('admin.categories.show');
        Route::post('/deletecategory/{id}',  'destroy')->name('admin.categories.destroy');
        Route::get('/categories/create',  'create')->name('admin.categories.create');
        Route::post('/category/store',  'store')->name('admin.category.store');
        Route::get('/category/edit/{id}',  'edit')->name('admin.categories.edit');
        Route::post('/category/update/{id}',  'update')->name('admin.category.update');
    });

    Route::prefix('admin')->controller(ProductController::class)->group(function () {
        Route::get('products', 'index')->name('admin.products');
        Route::post('/product/list', 'show')->name('admin.products.show');
        Route::get('/products/create', 'create')->name('admin.products.create');
        Route::post('/product/store','store')->name('admin.product.store');
    });
});